import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:freshman/screen/main/home_screen.dart';
import 'package:freshman/utils/color_definition.dart';
import 'package:freshman/utils/ui_tools.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String acc = "";
  String pass = "";

  doRegist() {}

  doForgot() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: appbar(
        //     title: "Work Hero", actions: [const Icon(Icons.message), space()]),
        body: Container(
      padding: const EdgeInsets.only(left: 15, right: 15),
      width: screenW,
      height: screenH,
      decoration: const BoxDecoration(color: white),
      child: ListView(children: [
        space(h: 100),
        img("logo.png"),
        space(),
        text(
          '請輸入帳號',
        ),
        input((v) {
          acc = v;
        }),
        space(),
        text(
          '請輸入密碼',
        ),
        input((v) {
          pass = v;
        }),
        space(h: 30),
        button('登入', () {
          print("acc:" + acc + "," + pass);
          if (acc == "111" && pass == "222") {
            show("登入成功");
            goto(context, const HomeScreen());
          } else {
            //show Error
            show("帳密有誤", isError: true);
          }
        }),
        space(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            gesture(
              text('註冊'),
              click: () {
                doRegist();
                toast("尚未開放註冊");
              },
            ),
            gesture(text('忘記密碼'), click: () {
              doForgot();
              toast('忘記密碼');
            })
          ],
        )
      ]),
    ));
  }
}
