import 'package:flutter/material.dart';
import 'package:freshman/screen/before_login/login_screen.dart';
import 'package:freshman/utils/ui_tools.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    screenW = MediaQuery.of(context).size.width;
    screenH = MediaQuery.of(context).size.height;

    Future.delayed(const Duration(seconds: 3), () {
      goto(context, const LoginScreen());
    });

    return const Scaffold(
        body: SizedBox(
            child: Center(
      child: CircularProgressIndicator(color: Colors.blue),
    )));
  }
}
