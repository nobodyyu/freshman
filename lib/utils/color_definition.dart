import 'package:flutter/material.dart';

const Color pink = Colors.pink;
const Color white = Colors.white;
const Color black = Colors.black54;
const Color blue = Colors.blue;
const Color primary = Color(0xff008BCD);
