import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:freshman/utils/color_definition.dart';

double screenW = 0;
double screenH = 0;

delay(int secs, Function fun) {
  Future.delayed(Duration(seconds: secs), fun());
}

goto(BuildContext context, Widget screen) {
  return Navigator.push(
      context,
      PageRouteBuilder(
          pageBuilder: (context, animation1, animation2) => screen,
          transitionDuration: Duration.zero,
          reverseTransitionDuration: Duration.zero));
}

Widget img(String imgName, {double w = 120, double h = 120}) {
  return Image.asset(
    "assets/images/$imgName",
    width: w,
    height: h,
  );
}

Widget space({double h = 15, double w = 15}) {
  return SizedBox(
    height: h,
    width: w,
  );
}

Widget text(String txt,
    {double size = 12, bool underline = false, Color color = Colors.black}) {
  return Text(txt,
      style: TextStyle(
          fontSize: size,
          color: color,
          decoration: (underline ? TextDecoration.underline : null)));
}

Widget button(String txt, Function fun) {
  return ElevatedButton(
      onPressed: () {
        fun();
      },
      child: Text(txt));
}

PreferredSizeWidget appbar(
    {String title = "",
    bool isGoback = false,
    List<Widget> actions = const []}) {
  return AppBar(
    backgroundColor: primary,
    leading: isGoback ? null : const SizedBox(),
    title: title == "" ? null : Text(title),
    centerTitle: true,
    actions: actions,
  );
}

Widget input(Function callBack, {TextInputType keyboard = TextInputType.text}) {
  return TextField(
    onChanged: (v) {
      callBack(v);
    },
    keyboardType: keyboard,
  );
}

show(String txt, {bool isError = false}) {
  isError ? EasyLoading.showError(txt) : EasyLoading.showSuccess(txt);
}

toast(String txt) {
  EasyLoading.showToast(txt);
}

GestureDetector gesture(Widget child, {required Null Function() click}) {
  return GestureDetector(
    behavior: HitTestBehavior.opaque,
    child: child,
    onTap: () {
      click();
    },
  );
}
